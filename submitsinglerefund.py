#!/usr/bin/env 
# usage: python submitsinglerefund.py -D apisinglerefund_data.csv

import random, csv, argparse, zeep, xmltodict, sys, re

def importdata(datafile):
	with open(datafile, 'rb') as csvfile:
		testcsv = csv.reader(csvfile, delimiter=',')
		next(testcsv) # skips header row
		for value in testcsv:
            testid = value [0]
			receiptnumber = value [1]
			amount = value[2]
			apiuser = value[3]
			apipassword = value[4]
            notes = value[7]

			constructxml(testid,receiptnumber,amount,apiuser,apipassword)

def constructxml(testid,receiptnumber,amount,apiuser,apipassword):
	requestdata = "<Refund><Receipt>" + receiptnumber + "</Receipt><Amount>" + amount + "</Amount><Security><UserName>" + apiuser + "</UserName><Password>" + apipassword + "</Password></Security></Refund>"
	
	makerequest(testid,requestdata)

def makerequest(testid,requestdata):
	wsdl = 'https://devsandbox.ippayments.com.au/interface/api/dts.asmx?WSDL'
	client = zeep.Client(wsdl)
	wsdlsplit = wsdl.rsplit('.',8)[0].rpartition('//')[2]
    # print("Testing Secure Remote API")
    # print('Please wait, processing refund ...')

    rspCode = "-1"
    rspReceipt = "-1"

    try:
        res = client.service.SubmitSingleRefund(requestdata)
        print 'Sent this: \n %s' % requestdata
        print 'SubmitSingleRefund: Got response of: \n %s' % res
        rspObj = xmltodict.parse(res)
        rspCode = rspObj['Response']['ResponseCode']
        rspReceipt = rspObj['Response']['Receipt']
    except Exception as e:
    	print e

    testcaseresults(res,requestdata,wsdlsplit)
    
    return (rspCode, rspReceipt)

def testcaseresults(testid,res,requestdata,wsdlsplit):
    getreceiptnum = re.findall(r'Receipt\W(\d{0,10})\D+', res)
    rescode = re.findall(r'ResponseCode\W(\d{0,1})\D+', res)
    originalreceipt = re.findall(r'Receipt\W(\d{0,10})\D+', requestdata) 
    getamount = re.findall(r'Amount\W(\d{0,100})\D+', requestdata)
    print '%s,%s,%s' % (originalreceipt[0],getreceiptnum[0],getamount[0])
    print 'Response Code is %s' % (rescode[0])
    
	with open('submitsinglerefund_' + wsdlsplit + '_results.txt','a') as f:
		f.write('Test ID: %s' % (testid))
		f.write('\n')
		# f.write(requestdata)
		# f.write(res.encode('utf-8'))
		f.write('Response Code is %s' % (rescode[0]))
		f.write('\n')
		f.close()

def main():
	parser = argparse.ArgumentParser('python submitsinglerefund.py -D <csvfile>')
	parser.add_argument('-D', '--datafile', help='CSV data file')
	args = parser.parse_args()

	datafile = args.datafile

	importdata(datafile)

if __name__ == '__main__':
	main()