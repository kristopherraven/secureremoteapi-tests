#!/usr/bin/env 
# usage: python submitsinglepayment.py -D apipayments_data.csv

from datetime import datetime
import random, csv, argparse, zeep, xmltodict, sys, re

def importdata(datafile):
	with open(datafile, 'rb') as csvfile:
		testcsv = csv.reader(csvfile, delimiter=',')
		next(testcsv) # skips header row
		for value in testcsv:
			testid = value[0]
			amount = value[1]
			cardnumber = value[2]
			expirymonth = value[3]
			expiryyear = value[4]
			apiuser = value[5]
			apipassword = value[6]
			notes = value[7]
			
			if 'Invalid custref' not in notes:
				custref = 'custref-%s' % random.randint(99999, 999999)
			else:
				custref = ''
			
			if 'Invalid custnumber' not in notes:
				custnumber = 'API-Purchase'
			else:
				custnumber = ''

			constructxml(testid,custnumber,custref,amount,cardnumber,expirymonth,expiryyear,apiuser,apipassword)

def constructxml(testid,custnumber,custref,amount,cardnumber,expirymonth,expiryyear,apiuser,apipassword):
	requestdata = "<Transaction><CustNumber>"+ custnumber +"</CustNumber><CustRef>" + custref + "</CustRef><Amount>" + amount + "</Amount><TrnType>1</TrnType><CreditCard><CardNumber>" + cardnumber + "</CardNumber><ExpM>" + expirymonth + "</ExpM><ExpY>" + expiryyear + "</ExpY></CreditCard><Security><UserName>" + apiuser + "</UserName><Password>" + apipassword + "</Password></Security></Transaction>"
	
	makerequest(testid,requestdata,custref)

def makerequest(testid,requestdata,custref):
	# need to add some code that logs responses and extracts the receiptnumber and amounts (used in other types like void, refund...)
	wsdl = 'https://demo.ippayments.com.au/interface/api/dts.asmx?WSDL'
	client = zeep.Client(wsdl)
	wsdlsplit = wsdl.rsplit('.',8)[0].rpartition('//')[2]
	# print("Testing Secure Remote API")
	print('Please wait, processing payment ...')

	rspCode = "-1"
	rspReceipt = "-1"

	try:
		res = client.service.SubmitSinglePayment(requestdata)
		# print 'Sent this: \n %s' % requestdata
		# print 'SubmitSinglePayment: Got response of: \n %s' % res
		rspObj = xmltodict.parse(res)
		rspCode = rspObj['Response']['ResponseCode']
		rspReceipt = rspObj['Response']['Receipt']
	except Exception as e:
		print e

	testcaseresults(testid,custref,res,requestdata,wsdlsplit)

	# return (rspCode, rspReceipt)

def testcaseresults(testid,custref,res,requestdata,wsdlsplit):
	getreceiptnum = re.findall(r'Receipt\W(\d{0,10})\D+', res)
	rescode = re.findall(r'ResponseCode\W(\d{0,10})\D+', res)
	declinemsg = re.findall(r'DeclinedMessage\W([a-zA-Z ]+)\W+', res)
	if declinemsg == []:
		declinemsg = '-'
	else:
		declinemsg = declinemsg
	# 	try:
	# except IndexError:
	# 	declinemsg = 'Passed'
	# 	continue
	# can construct the next CSV file here (used for Refund, Void...)
	print '%s,%s' % (custref,getreceiptnum[0])
	# print 'Response Code is %s' % (rescode[0])

	with open('submitsinglepayment_' + wsdlsplit + '_results.txt','a') as f:
		f.write('Test ID: %s' % (testid))
		f.write('\n')
		f.write(requestdata)
		f.write('\n')
		f.write(res.encode('utf-8'))
		# Need to fix the code below....
		f.write('Response Code is %s. Decline Message is: %s' % (rescode[0],declinemsg[0]))
		# f.write('Response Code is %s' % (rescode[0]))
		f.write('\n')
		f.close()

def main():
	parser = argparse.ArgumentParser('python submitsinglepayment.py -D <csvfile>')
	parser.add_argument('-D', '--datafile', help='CSV data file')
	args = parser.parse_args()

	datafile = args.datafile

	importdata(datafile)

if __name__ == '__main__':
	main()