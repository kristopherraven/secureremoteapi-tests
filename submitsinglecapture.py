#!/usr/bin/env 
# usage: python submitsinglecapture.py -D apisinglecapture_data.csv

import random, csv, argparse, zeep, xmltodict, sys, re

def importdata(datafile):
	with open(datafile, 'rb') as csvfile:
		testcsv = csv.reader(csvfile, delimiter=',')
		next(testcsv) # skips header row
		for value in testcsv:
			amount = value[0]
			cardnumber = value[1]
			expirymonth = value[2]
			expiryyear = value[3]
			apiuser = value[4]
			apipassword = value[5]
			custref = 'custref-%s' % random.randint(99999, 999999)
			constructxml(custref,amount,cardnumber,expirymonth,expiryyear,apiuser,apipassword)

def constructxml(custref,amount,cardnumber,expirymonth,expiryyear,apiuser,apipassword):
	requestdata = "<Capture><Receipt>"+ receiptnum +"</Receipt><Amount>"+ amount +"</Amount><Security><UserName>"+ apiuser +"</UserName><Password>"+ apipassword +"</Password></Security></Capture>"
	
	makerequest(requestdata,custref)

def makerequest(requestdata,custref):
# need to add some code that logs responses and extracts the receiptnumber and amounts (used in other types like void, refund...)
	wsdl = 'https://devsandbox.ippayments.com.au/interface/api/dts.asmx?WSDL'
	client = zeep.Client(wsdl)
	wsdlsplit = wsdl.rsplit('.',8)[0].rpartition('//')[2]
	# print("Testing Secure Remote API")
	# print('Please wait, processing payment ...')

	rspCode = "-1"
	rspReceipt = "-1"

	try:
		res = client.service.SubmitSingleCapture(requestdata)
		# print 'Sent this: \n %s' % requestdata
		print 'SubmitSingleCapture: Got response of: \n %s' % res
		rspObj = xmltodict.parse(res)
		rspCode = rspObj['Response']['ResponseCode']
		rspReceipt = rspObj['Response']['Receipt']
	except Exception as e:
		print e

	testcaseresults(custref,res,requestdata,wsdlsplit)

	return (rspCode, rspReceipt)


def testcaseresults(custref,res,requestdata,wsdlsplit):
# can construct the next CSV file here (used for Refund, Void...)
	getreceiptnum = re.findall(r'Receipt\W(\d{0,10})\D+', res)
	rescode = re.findall(r'ResponseCode\W(\d{0,10})\D+', res)
	getamount = re.findall(r'Amount\W(\d{0,100})\D+', requestdata)
	print '%s,%s,%s' % (custref,getreceiptnum[0],getamount[0])
	print 'Response Code is %s' % (rescode[0])

	with open('submitsinglecapture_results.txt','a') as f:
		f.write('Test ID: %s' % (testid))
		f.write('\n')
		# f.write(requestdata)
		# f.write(res.encode('utf-8'))
		f.write('Response Code is %s' % (rescode[0]))
		f.write('\n')
		f.close()

def main():
	parser = argparse.ArgumentParser('python submitsinglecapture.py -D <csvfile>')
	parser.add_argument('-D', '--datafile', help='CSV data file')
	args = parser.parse_args()

	datafile = args.datafile

	importdata(datafile)

if __name__ == '__main__':
	main()