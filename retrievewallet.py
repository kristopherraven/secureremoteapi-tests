#!/usr/bin/env 
# usage: python retrievewallet.py -D apiretrievewallet_data.csv

import random, csv, argparse, zeep, xmltodict, sys, re

def importdata(datafile):
	with open(datafile, 'rb') as csvfile:
		testcsv = csv.reader(csvfile, delimiter=',')
		next(testcsv) # skips header row
		for value in testcsv:
			accountnum = value[0]
			custref = value[1]
			passtype = value[2]
			pairingtoken = value[3]
			walletmerchid = value[4]
			apiuser = value[5]
			apipassword = value[6]
			# custref = 'custref-%s' % random.randint(99999, 999999)
			constructxml(accountnum,custref,passtype,pairingtoken,walletmerchid,apiuser,apipassword)

def constructxml(accountnum,custref,passtype,pairingtoken,walletmerchid,apiuser,apipassword):
	requestdata = "<Transaction><AccountNumber>"+ accountnum +"</AccountNumber><CustRef>" + custref + "</CustRef><Wallet><Type>"+ passtype +"</Type><PairingToken>"+ pairingtoken +"</PairingToken><WalletMerchantID>" + walletmerchid +"</WalletMerchantID></Wallet><Security><UserName>" + apiuser + "</UserName><Password>" + apipassword + "</Password></Security></Transaction>"

	makerequest(requestdata,custref)

def makerequest(requestdata,custref):
# need to add some code that logs responses and extracts the receiptnumber and amounts (used in other types like void, refund...)
	client = zeep.Client('https://demo.ippayments.com.au/interface/api/dts.asmx?WSDL')

	print("Testing Secure Remote API")
	print('Please wait, processing payment ...')

	rspCode = "-1"
	rspReceipt = "-1"

	try:
		res = client.service.RetrieveWallet(requestdata)
		print 'Sent this: \n %s' % requestdata
		print 'RetrieveWallet: Got response of: \n %s' % res
		rspObj = xmltodict.parse(res)
		rspCode = rspObj['Response']['ResponseCode']
		rspReceipt = rspObj['Response']['Receipt']
	except Exception as e:
		print e

	# testcaseresults(custref,res,requestdata)

	return (rspCode, rspReceipt)


# def testcaseresults(custref,res,requestdata):
	# # can construct the next CSV file here (used for Refund, Void...)
	# rescode = re.findall(r'ResponseCode\W(\d{0,10})\D+', res)
	# getamount = re.findall(r'Amount\W(\d{0,100})\D+', requestdata)
	# print '%s,%s,%s' % (custref,getreceiptnum[0],getamount[0])
	# print 'Response Code is %s' % (rescode[0])

def main():
	parser = argparse.ArgumentParser('python retrievewallet.py -D <csvfile>')
	parser.add_argument('-D', '--datafile', help='CSV data file')
	args = parser.parse_args()

	datafile = args.datafile

	importdata(datafile)

if __name__ == '__main__':
	main()