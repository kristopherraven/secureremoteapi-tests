#!/usr/bin/env 
# usage: python submitsinglevoid.py -D apivoid_data.csv

import random, csv, argparse, zeep, xmltodict, sys, re

def importdata(datafile):
	with open(datafile, 'rb') as csvfile:
		testcsv = csv.reader(csvfile, delimiter=',')
		next(testcsv) # skips header row
		for value in testcsv:
			receiptnumber = value [0]
			amount = value[1]
			apiuser = value[2]
			apipassword = value[3]
			constructxml(receiptnumber,amount,apiuser,apipassword)

def constructxml(receiptnumber,amount,apiuser,apipassword):

	requestdata = "<Void><Receipt>" + receiptnumber + "</Receipt><Amount>" + amount + "</Amount><Security><UserName>" + apiuser + "</UserName><Password>" + apipassword + "</Password></Security></Void>"

	makerequest(requestdata)

def makerequest(requestdata):
    client = zeep.Client('https://demo.ippayments.com.au/interface/api/dts.asmx?WSDL')

    # print("Testing Secure Remote API")
    # print('Please wait, processing void ...')

    rspCode = "-1"
    rspReceipt = "-1"

    try:
        res = client.service.SubmitSingleVoid(requestdata)
        print 'Sent this: \n %s' % requestdata
        print 'SubmitSingleVoid: Got response of: \n %s' % res
        rspObj = xmltodict.parse(res)
        rspCode = rspObj['Response']['ResponseCode']
        rspReceipt = rspObj['Response']['Receipt']
    except Exception as e:
    	print e

    testcaseresults(res,requestdata)
    
    return (rspCode, rspReceipt)

def testcaseresults(res,requestdata):
    # getreceiptnum = re.findall(r'Receipt\W(\d{0,1})\D+', res)
    rescode = re.findall(r'ResponseCode\W(\d{0,1})\D+', res)
    originalreceipt = re.findall(r'Receipt\W(\d{0,10})\D+', requestdata) 
    getamount = re.findall(r'Amount\W(\d{0,100})\D+', requestdata)
    # print '%s,%s,%s' % (originalreceipt[0],getreceiptnum[0],getamount[0])
    print '%s,%s' % (originalreceipt[0],getamount[0])
    print 'Response is %s' % (rescode[0])

def main():
	parser = argparse.ArgumentParser('python submitsinglevoid.py -D <csvfile>')
	parser.add_argument('-D', '--datafile', help='CSV data file')
	args = parser.parse_args()

	datafile = args.datafile

	importdata(datafile)

if __name__ == '__main__':
	main()